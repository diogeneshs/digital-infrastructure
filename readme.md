# Diogenes Tech Ops

![](deployment.png)

The above diagram contains two kvms. 
- The data-kvm will be decoupled, so it can easily be added elsewhere and made redundant
- The App kvm will run the compute docker containers, helios, homepage(ngnix), and erpnext on ZFS.
- The config files will There will also be a cron job that manages backups with rsync 

## Backups
The following sequence illustrates the automated backup process:
![](sequence.svg)

## Maintenance Schedule

Rob, Joe and Charles, commit to maintaining this system on rotating basis.  The min chore is testing the backups through verifying the restore process. During the month they are assigned they will troubleshoot any problems. 